#include <openssl/evp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define IV_LEN 16
#define KEY_LEN 16

unsigned char SM4Key[KEY_LEN];

//字节数组 转 十六进制字符串 
void Byte2HexStr(const unsigned char* source, char* dest, int sourceLen);
//十六进制字符串转字节数组 
void hex_str_to_byte(const char *in, int len, unsigned char *out); 
