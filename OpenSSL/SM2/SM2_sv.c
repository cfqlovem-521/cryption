#include <stdio.h>
#include <stdlib.h>
#include <openssl/ec.h>
#include <openssl/evp.h>

int SM2_sign(EVP_PKEY* pkey,const char *sourcefilename,const char *sigfilename){
	/* compute SM2 signature */
	EVP_PKEY_set_alias_type(pkey, EVP_PKEY_SM2);

    EVP_PKEY_CTX *ctx = EVP_PKEY_CTX_new(pkey, NULL);
    EVP_MD_CTX *mctx = EVP_MD_CTX_new();
    EVP_MD_CTX_set_pkey_ctx(mctx, ctx);

    EVP_DigestSignInit(mctx, NULL, EVP_sm3(), NULL, pkey);
	
	//打开源文件，计算文件的哈希值 
	 FILE *fp2 = fopen(sourcefilename,"rb");
    int n = 0;
    unsigned char buffer[1024];
    while( (n= fread(buffer,1,sizeof(buffer),fp2))>0){
        EVP_DigestSignUpdate(mctx, buffer, n);
    }
    fclose(fp2);
	
	//计算文件的签名值 
	size_t sig_len;
	EVP_DigestSignFinal(mctx, NULL, &sig_len);
	unsigned char* sig = (unsigned char*)malloc(sig_len);
	EVP_DigestSignFinal(mctx, sig, &sig_len);
 	
 	//打印签名值长度和签名值
 	printf("签名值长度：%d\n",sig_len);
 	printf("签名值：");
	for(int i=0;i<sig_len;i++){
		printf("%02x",sig[i]);
	}
	printf("\n"); 
 	//将文件的签名值和长度 写入到输出文件 
 	FILE *fp = fopen(sigfilename,"wb");
 	fwrite(&sig_len,sizeof(sig_len),1,fp);
 	fwrite(sig,1,sig_len,fp);
 	fflush(fp);
 	fclose(fp);
 	
 	//释放资源 
 	free(sig);
 	EVP_MD_CTX_free(mctx);
 	EVP_PKEY_CTX_free(ctx);
 	return 1;
}

int SM2_verify(EVP_PKEY* pkey,const char *sourcefile,const char *sigfilename){
	
	//打开存储签名值的文件，读出签名值 
	FILE *fp = fopen(sigfilename,"rb");
	size_t sig_len;
	fread(&sig_len,sizeof(sig_len),1,fp);
	unsigned char *sig = (unsigned char*)malloc(sig_len);
	fread(sig,1,sig_len,fp);
	fclose(fp);
	
	/* verify SM2 signature */
	EVP_PKEY_set_alias_type(pkey, EVP_PKEY_SM2);
	
	EVP_PKEY_CTX *ctx = EVP_PKEY_CTX_new(pkey, NULL);
	EVP_MD_CTX *mctx = EVP_MD_CTX_new();
	EVP_MD_CTX_set_pkey_ctx(mctx, ctx);

	EVP_DigestVerifyInit(mctx, NULL, EVP_sm3(), NULL, pkey);
	
	//打开源文件，计算哈希值	
	FILE *fp2 = fopen(sourcefile,"rb");
	int n = 0;
	unsigned char buffer[1024];
	while( (n= fread(buffer,1,sizeof(buffer),fp2))>0){
		EVP_DigestVerifyUpdate(mctx, buffer, n);
	}
	fclose(fp2);
	
	//计算签名值，并和源签名值比对，验签 
	int ret = 0;
	if ((EVP_DigestVerifyFinal(mctx, sig, sig_len)) != 1 ){
		printf("Verify SM2 signature failed!\n");
		ret = 0;
	}else{
		printf("Verify SM2 signature succeeded!\n");
		ret = 1;
	}
	fflush(stdout);
	
	//释放资源 
	free(sig);
	EVP_MD_CTX_free(mctx);
 	EVP_PKEY_CTX_free(ctx);
 	return ret;
}

int main(int argc,const char *argv[])
{
//	unsigned char message[16] = { 0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7,
//				    0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF };
//	size_t message_len = sizeof(message);
 
	/* create SM2 Ellipse Curve parameters and key pair */
	EVP_PKEY_CTX *pctx = EVP_PKEY_CTX_new_id(EVP_PKEY_EC, NULL);
	EVP_PKEY_paramgen_init(pctx);
	EVP_PKEY_CTX_set_ec_paramgen_curve_nid(pctx, NID_sm2);

	EVP_PKEY* pkey = EVP_PKEY_new();
	EVP_PKEY_keygen_init(pctx);
	EVP_PKEY_keygen(pctx, &pkey);
	
	SM2_sign(pkey,argv[1],argv[2]);
	SM2_verify(pkey,argv[1],argv[2]);
	
	EVP_PKEY_free(pkey);
	EVP_PKEY_CTX_free(pctx);
	return 0;
}
