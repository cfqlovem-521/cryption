#include <openssl/evp.h>
#include <openssl/ec.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int SM2_genkey(EVP_PKEY *sm2_key){
	/* create SM2 Ellipse Curve parameters and key pair */
 	EVP_PKEY_CTX *pctx = EVP_PKEY_CTX_new_id(EVP_PKEY_EC, NULL);
 	EVP_PKEY_paramgen_init(pctx);
	EVP_PKEY_CTX_set_ec_paramgen_curve_nid(pctx, NID_sm2);
	
	EVP_PKEY_keygen_init(pctx);
	EVP_PKEY_keygen(pctx, &sm2_key);
	EVP_PKEY_CTX_free(pctx);
	return 1;
}

int SM2_destroy(EVP_PKEY *sm2_key){
	EVP_PKEY_free(sm2_key);
	return 1; 
}

int SM2_encrypt(EVP_PKEY *pkey,unsigned char *data,size_t data_len,const char *outfile){
 
	EVP_PKEY_set_alias_type(pkey, EVP_PKEY_SM2);
	EVP_PKEY_CTX *ectx = EVP_PKEY_CTX_new(pkey, NULL);
	EVP_PKEY_encrypt_init(ectx);
	
 	size_t outlen;
	EVP_PKEY_encrypt(ectx, NULL, &outlen, data, data_len);
	unsigned char *outbuf = (unsigned char *)malloc(outlen);
	EVP_PKEY_encrypt(ectx, outbuf, &outlen, data, data_len);
	
	printf("Ciphertext length: %ld bytes.\n", outlen);
	
	FILE *fp = fopen(outfile,"wb");
	fwrite(&outlen,1,sizeof(outlen),fp);
	fwrite(outbuf,1,outlen,fp);
	fflush(fp);
	fclose(fp);
	free(outbuf);
	EVP_PKEY_CTX_free(ectx);
	return 1;
}

int SM2_decrypt(EVP_PKEY *pkey,unsigned char *data,size_t data_len,unsigned char *plain){
	
	EVP_PKEY_set_alias_type(pkey, EVP_PKEY_SM2);
	EVP_PKEY_CTX *ectx = EVP_PKEY_CTX_new(pkey, NULL);
	EVP_PKEY_decrypt_init(ectx);

	size_t outlen;
	EVP_PKEY_decrypt(ectx, NULL, &outlen, data, data_len);
	unsigned char *outbuf = (unsigned char *)malloc(outlen);
	EVP_PKEY_decrypt(ectx, outbuf, &outlen, data, data_len);
	
	for(int i=0;i<outlen;i++){
		plain[i] = outbuf[i];
	}
	
	EVP_PKEY_CTX_free(ectx);
	return 1;
}

int main(int argc,const char *argv[])
{
	unsigned char message[16] = {0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70,
				  0x80, 0x90, 0xA0, 0xB0, 0xC0, 0xD0, 0xE0, 0xF0};
	size_t message_len = sizeof(message);
	
	printf("原始数据：128位，16进制\n");
	for(int i=0;i<16;i++){
		printf("%02x",message[i]);
	}
	printf("\n\n");
	
	printf("SM2加密数据输出到了 ==> %s\n\n",argv[1]); 
	
	EVP_PKEY_CTX *pctx = EVP_PKEY_CTX_new_id(EVP_PKEY_EC, NULL);
 	EVP_PKEY_paramgen_init(pctx);
	EVP_PKEY_CTX_set_ec_paramgen_curve_nid(pctx, NID_sm2);
	
	EVP_PKEY *pkey = NULL;
	EVP_PKEY_keygen_init(pctx);
	EVP_PKEY_keygen(pctx, &pkey);
	
	SM2_encrypt(pkey,message,message_len,argv[1]);
	
	//读取密文，解密 
	FILE *fp = fopen(argv[1],"rb");
	size_t buflen;
	fread(&buflen,sizeof(buflen),1,fp);
	printf("buflen = %d\n",buflen);
	unsigned char buffer[buflen];
	fread(buffer,1,buflen,fp);
	
	unsigned char plain[16]; 
	SM2_decrypt(pkey,buffer,buflen,plain);
	
	printf("SM2解密数据：\n");
	for(int i=0;i<16;i++){
		printf("%02x",plain[i]);
	}
	printf("\n\n");
	
	EVP_PKEY_free(pkey);
	return 0;
}
