#include <openssl/evp.h>  
#include <string.h>
#include <stdio.h> 
#include <stdlib.h>

/*
		EVP_ENCODE_CTX *EVP_ENCODE_CTX_new(void);
        void EVP_ENCODE_CTX_free(EVP_ENCODE_CTX *ctx);
        int EVP_ENCODE_CTX_copy(EVP_ENCODE_CTX *dctx, EVP_ENCODE_CTX *sctx);
        int EVP_ENCODE_CTX_num(EVP_ENCODE_CTX *ctx);
        void EVP_EncodeInit(EVP_ENCODE_CTX *ctx);
        int EVP_EncodeUpdate(EVP_ENCODE_CTX *ctx, unsigned char *out, int *outl,
                             const unsigned char *in, int inl);
        void EVP_EncodeFinal(EVP_ENCODE_CTX *ctx, unsigned char *out, int *outl);
        int EVP_EncodeBlock(unsigned char *t, const unsigned char *f, int n);

        void EVP_DecodeInit(EVP_ENCODE_CTX *ctx);
        int EVP_DecodeUpdate(EVP_ENCODE_CTX *ctx, unsigned char *out, int *outl,
                             const unsigned char *in, int inl);
        int EVP_DecodeFinal(EVP_ENCODE_CTX *ctx, unsigned char *out, int *outl);
        int EVP_DecodeBlock(unsigned char *t, const unsigned char *f, int n);

*/

int base64_encode_newline(const char *infile,const char *outfile)
//按照 openssl 标准 ，每64个字符，添加一个换行。
//如果不想换行，把outbuf[65] ==> outbuf[64]。 并放开注释。 
{
	FILE *fp1 = fopen(infile,"rb");
	FILE *fp2 = fopen(outfile,"wb");
	
	EVP_ENCODE_CTX *ectx = EVP_ENCODE_CTX_new();
	EVP_EncodeInit(ectx);
	
	int inlen = 0;
	int outlen = 0;
	unsigned char inbuf[48];
	unsigned char outbuf[65];
	while((inlen = fread(inbuf,1,sizeof(inbuf),fp1))){
		EVP_EncodeUpdate(ectx, outbuf, &outlen, inbuf, inlen);
//		if(outlen != 0){
//			outlen--;
//		}
		fwrite(outbuf,1,outlen,fp2);
		fflush(fp2);	
	}
	
	int finlen;
	EVP_EncodeFinal(ectx,outbuf,&finlen);
	fwrite(outbuf,1,finlen,fp2);
	fflush(fp2);
	
	fclose(fp1);
	fclose(fp2);
	
	EVP_ENCODE_CTX_free(ectx);
	return 0;
}

 
int base64_decode_newline(const char *infile,const char *outfile)
//按照 openssl 标准，每行 64 字符 + 1个换行符，进行解码 
//如果源文件没有换行，把inbuf[65] ==> inbuf[64]。
{
	FILE *fp1 = fopen(infile,"rb");
	FILE *fp2 = fopen(outfile,"wb");
	
	EVP_ENCODE_CTX *ectx = EVP_ENCODE_CTX_new();
	EVP_DecodeInit(ectx);
	
	int inlen = 0;
	int outlen = 0;
	unsigned char inbuf[65];
	unsigned char outbuf[48];
	while((inlen = fread(inbuf,1,sizeof(inbuf),fp1))){
		EVP_DecodeUpdate(ectx, outbuf, &outlen, inbuf, inlen);
		fwrite(outbuf,1,outlen,fp2);
		fflush(fp2);	
	}
	
	int finlen;
	EVP_DecodeFinal(ectx,outbuf,&finlen);
	fwrite(outbuf,1,finlen,fp2);
	fflush(fp2);
	
	fclose(fp1);
	fclose(fp2);
	
	EVP_ENCODE_CTX_free(ectx);
	return 0;
}

int base64_encode(const char *infile,const char *outfile)
//按照 openssl 标准 ，每64个字符，添加一个换行。
//如果不想换行，把outbuf[65] ==> outbuf[64]。 并放开注释。 
{
	FILE *fp1 = fopen(infile,"rb");
	FILE *fp2 = fopen(outfile,"wb");
	
	EVP_ENCODE_CTX *ectx = EVP_ENCODE_CTX_new();
	EVP_EncodeInit(ectx);
	
	int inlen = 0;
	int outlen = 0;
	unsigned char inbuf[48];
	unsigned char outbuf[64];
	while((inlen = fread(inbuf,1,sizeof(inbuf),fp1))){
		EVP_EncodeUpdate(ectx, outbuf, &outlen, inbuf, inlen);
		if(outlen != 0){
			outlen--;
		}
		fwrite(outbuf,1,outlen,fp2);
		fflush(fp2);	
	}
	
	int finlen;
	EVP_EncodeFinal(ectx,outbuf,&finlen);
	fwrite(outbuf,1,finlen,fp2);
	fflush(fp2);
	
	fclose(fp1);
	fclose(fp2);
	
	EVP_ENCODE_CTX_free(ectx);
	return 0;
}

 
int base64_decode(const char *infile,const char *outfile)
//按照 openssl 标准，每行 64 字符 + 1个换行符，进行解码 
//如果源文件没有换行，把inbuf[65] ==> inbuf[64]。
{
	FILE *fp1 = fopen(infile,"rb");
	FILE *fp2 = fopen(outfile,"wb");
	
	EVP_ENCODE_CTX *ectx = EVP_ENCODE_CTX_new();
	EVP_DecodeInit(ectx);
	
	int inlen = 0;
	int outlen = 0;
	unsigned char inbuf[64];
	unsigned char outbuf[48];
	while((inlen = fread(inbuf,1,sizeof(inbuf),fp1))){
		EVP_DecodeUpdate(ectx, outbuf, &outlen, inbuf, inlen);
		fwrite(outbuf,1,outlen,fp2);
		fflush(fp2);	
	}
	
	int finlen;
	EVP_DecodeFinal(ectx,outbuf,&finlen);
	fwrite(outbuf,1,finlen,fp2);
	fflush(fp2);
	
	fclose(fp1);
	fclose(fp2);
	
	EVP_ENCODE_CTX_free(ectx);
	return 0;
}

int main(int argc,const char *argv[])
{
	if(strcmp(argv[1],"-e")==0){
		base64_encode_newline(argv[2],argv[3]);
	}else if(strcmp(argv[1],"-d")==0){
		base64_decode_newline(argv[2],argv[3]);
	}else if(strcmp(argv[1],"-ne")==0){
		base64_encode(argv[2],argv[3]);
	}else if(strcmp(argv[1],"-nd")==0){
		base64_decode(argv[2],argv[3]);
	}else{
		printf("如果没有换行，请加参数 n \n");
		printf("encode usage: ./a.out -(n)e infile outfile\n");
		printf("decode usage: ./a.out -(n)d infile outfile\n");
	}
	
	return 0;
}
