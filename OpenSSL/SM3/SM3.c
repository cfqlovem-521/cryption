#include <stdio.h>
#include <stdlib.h>
#include <openssl/evp.h>

#define SM3_DIGEST_LENGTH 32
#define MAX_DIGEST_LENGTH 64

int SM3_digest(const char *algorithms,const char *infile,const char *outfile){
	FILE *fp1 = fopen(infile,"rb");
	FILE *fp2 = fopen(outfile,"wb");
	if(fp1==NULL || fp2==NULL){
		return 1;
	}

	EVP_MD_CTX *evpCtx = EVP_MD_CTX_new();
	const EVP_MD *md = EVP_get_digestbyname(algorithms);
	if(md==NULL){
		return 2;
	}
    EVP_DigestInit_ex(evpCtx,md, NULL);
    
    unsigned char inbuf[1024] = {0};
    int inlen = 0;
	while((inlen = fread(inbuf,1,sizeof(inbuf),fp1))){
		EVP_DigestUpdate(evpCtx,inbuf,inlen);
	}    

	unsigned char result[MAX_DIGEST_LENGTH] = {0};
	int result_len=0;
    EVP_DigestFinal_ex(evpCtx, result, &result_len);

	fwrite(result,1,result_len,fp2);
	fflush(fp2);
	
	fclose(fp1);
	fclose(fp2); 
	EVP_MD_CTX_free(evpCtx);
	return 0;
} 

int main(int argc,const char* argv[]){
	int r = SM3_digest(argv[1],argv[2],argv[3]);
	if(r==1){
		printf("unknown file %s\n",argv[2]);
		printf("usage: ./a.out sm3 infile outfile");
	}else if(r==2){
		printf("unknown algorithms %s\n",argv[1]);
		printf("usage: ./a.out sm3 infile outfile");
		printf("support : SM3 | SHA256 | SHA512 | SHA224 | SHA384 | MD5\n");
	}
	return 0;	
}
