#include <gmp.h>

int main(){
	//init
	mpz_t base,result;
	mpz_init_set_ui(base,2);
	mpz_init(result); 
	//pow
	mpz_pow_ui(result, base, 1206);
	//print
	gmp_printf("%Zd\n", result);
	//free
	mpz_clear(base);
	mpz_clear(result);
	return 0;
} 
