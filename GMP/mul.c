#include <gmp.h>

int main(){
	//init
	mpz_t result;
	mpz_init_set_ui(result,1);

	//mul
	for(int i=0;i<10;i++){
		mpz_mul_ui(result, result, 20191202+i);
	}
	
	//print
	gmp_printf("%Zd\n", result);
	//free

	mpz_clear(result);
	return 0;
}
