#include "sdf.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <openssl/rand.h>
#include <openssl/evp.h>

int main(){
	int ret;
	void ** phDeviceHandle;
	phDeviceHandle = (void **) malloc(20);
	
	void ** phSessionHandle;
	phSessionHandle = (void **) malloc(20);
	
	DEVICEINFO stDeviceInfo;
	ret = SDF_OpenDevice(phDeviceHandle);
	handleError(ret);
	printf("Device opened!\n");
	
	ret = SDF_OpenSession(*phDeviceHandle, phSessionHandle);
	handleError(ret);
	printf("Session established!\n");
	
	SDF_GetDeviceInfo(*phSessionHandle, &stDeviceInfo);
	handleError(ret);
	printf("Issuer Name: %s\n", stDeviceInfo.IssuerName);
    printf("Device Name: %s\n", stDeviceInfo.DeviceName);
    printf("Device Serial: %s\n", stDeviceInfo.DeviceSerial);
    printf("Device Version: %d\n", stDeviceInfo.DeviceVersion);
	printf("Device StandardVersion: %d\n",stDeviceInfo.StandardVersion);
	printf("Device AsymAlgAbility: %d %d\n",stDeviceInfo.AsymAlgAbility[0],stDeviceInfo.AsymAlgAbility[1]);
	printf("Device SymAlgAbilty: %d\n",stDeviceInfo.SymAlgAbilty);
	printf("Device HashAlgAbility: %d\n",stDeviceInfo.HashAlgAbility);
	printf("Device BufferSize: %d Bytes\n",stDeviceInfo.BufferSize);
	
	unsigned char key[SM4_KEY_LEN];
	SDF_GenerateRandom(*phSessionHandle, SM4_KEY_LEN, key);
	printf("\nGenerating random 16 bytes key := \n");
	for(int i=0;i<SM4_KEY_LEN;i++){
		printf("%02x",key[i]);
	}
	printf("\n\n");
	
//	int SDF_HashInit(*phSessionHandle, unsigned int uiAlgID,ECCrefPublicKey * pucPublicKey, unsigned char * pucID,unsigned int uiIDLength);
	
	SDF_CloseSession(*phSessionHandle);
	handleError(ret);
	printf("Session destroyed!\n");
	
	SDF_CloseDevice(*phDeviceHandle);
	handleError(ret);
	printf("Device closed!\n");
	
	return 0;
}
