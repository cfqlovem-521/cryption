#include "sdf.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <openssl/rand.h>
#include <openssl/evp.h>

EVP_MD_CTX *Evp_Md_Ctx;

void handleError(int code){
	if(code == SDR_OK){
		return;
	}else{
		printf("Error Code = %d\n",code);
		exit(code);
	}
}
//********************************
//设备管理
//********************************

int SDF_OpenDevice(void ** phDeviceHandle){
	*phDeviceHandle = (void*)"20191206";
	return SDR_OK;
}


int SDF_CloseDevice(void *hDeviceHandle){
	if(hDeviceHandle == NULL){
		return SDR_OPENDEVICE;
	}
	hDeviceHandle = NULL;
	return SDR_OK;
}

int SDF_OpenSession(void * hDeviceHandle, void ** phSessionHandle){
	if(hDeviceHandle == NULL){
		return SDR_OPENSESSION;
	}
	*phSessionHandle = (void *)"session_20191206";
	return SDR_OK;
}

int SDF_CloseSession(void * hSessionHandle){
	if(hSessionHandle == NULL){
		return SDR_OPENSESSION;
	}
	hSessionHandle = NULL;
	return SDR_OK;
}

int SDF_GetDeviceInfo(void * hSessionHandle, DEVICEINFO * pstDeviceInfo){
	if(hSessionHandle == NULL){
		return SDR_OPENSESSION;
	}
	
	strcpy((char *)pstDeviceInfo->IssuerName,"SDF_20191206CFQ");
	strcpy((char *)pstDeviceInfo->DeviceName,"SDFBESTI1912");
	strcpy((char *)pstDeviceInfo->DeviceSerial,"2022042114");
	pstDeviceInfo->DeviceVersion = 1;
	pstDeviceInfo->StandardVersion = 1;
	pstDeviceInfo->AsymAlgAbility[0] = 0;
	pstDeviceInfo->AsymAlgAbility[1] = 0;
	pstDeviceInfo->SymAlgAbilty = 0;
	pstDeviceInfo->HashAlgAbility = 0;
	pstDeviceInfo->BufferSize = 48 * 1024;
	
	return SDR_OK;
}


int SDF_GenerateRandom(void * hSessionHandle, unsigned int uiLength, unsigned char * pucRandom){
	if(hSessionHandle == NULL){
		return SDR_OPENSESSION;
	}
    int ret = RAND_bytes(pucRandom,uiLength);
    RAND_cleanup();
    
	if(ret!=1){
		printf("SDF_GenerateRandom Error\n");
	  	return SDR_RANDERR;
 	}
	return SDR_OK;
}

int SDF_HashInit(void * hSessionHandle, unsigned int uiAlgID,ECCrefPublicKey * pucPublicKey, unsigned char * pucID,unsigned int uiIDLength){
	if(hSessionHandle == NULL){
		return SDR_OPENSESSION;
	}
	Evp_Md_Ctx = EVP_MD_CTX_new();
	const EVP_MD *Evp_Md = EVP_get_digestbyname("SM3");
	EVP_DigestInit_ex(Evp_Md_Ctx,Evp_Md, NULL);
	return SDR_OK;
}

int SDF_HashUpdate(void * hSessionHandle, unsigned char * pucData, unsigned int uiDataLength){
	if(hSessionHandle == NULL){
		return SDR_OPENSESSION;
	}
	EVP_DigestUpdate(Evp_Md_Ctx,pucData,uiDataLength);
	return SDR_OK;
}

int SDF_HashFinal(void * hSessionHandle, unsigned char * pucHash, unsigned int * puiHashLength){
	if(hSessionHandle == NULL){
		return SDR_OPENSESSION;
	}
	EVP_DigestFinal_ex(Evp_Md_Ctx, pucHash, puiHashLength);
	EVP_MD_CTX_free(Evp_Md_Ctx);
	return SDR_OK;
}
