#include "../skfapi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define	TRUE	1
#define FALSE	0

typedef  struct handles{
    HANDLE hdev;
    HANDLE happ;
    HANDLE hkey;
    HANDLE hcont;
    ECCPUBLICKEYBLOB	ecc_pub;
	ECCSIGNATUREBLOB	ecc_sign;
	ULONG	ulEccpubLen;		// = sizeof(ECCPUBLICKEYBLOB)
	PENVELOPEDKEYBLOB key_pair;
}GM3000_SM2SV_Handles;

ULONG SM3_digest(GM3000_SM2SV_Handles *handles,const char *filename,BYTE digest[32]){
	
	ULONG ulRslt = SAR_OK;
	HANDLE hhash;
	
	//摘要初始化，不用密钥，传NULL即可，不影响 
	ulRslt = SKF_DigestInit(handles->hdev, SGD_SM3, NULL, NULL, 0, &hhash);
	
	//循环读取文件内容，摘要迭代 
	FILE *fp = fopen(filename,"rb");
	int inlen=0;
	BYTE inbuf[1024];
	while((inlen = fread(inbuf,1,sizeof(inbuf),fp))>0){
		ulRslt = SKF_DigestUpdate(hhash, inbuf,inlen);
	}
	fclose(fp);
	
	//输出最终摘要结果，256bit , 32Byte 
	ULONG outlen;
	ulRslt = SKF_DigestFinal (hhash, NULL, &outlen); 
	if(outlen != 32){
		printf("hash failed\n");
		return ulRslt;
	}else{
		printf("outlen = %lu\n",outlen);
	}
	BYTE *outbuf = (BYTE *)malloc(outlen);
	ulRslt = SKF_DigestFinal (hhash, outbuf, &outlen);
	
	for(int i=0;i<32;i++){
		digest[i] = outbuf[i];
	}
	
	return ulRslt;
}

ULONG SM2_SV_destroy(GM3000_SM2SV_Handles *handles){
	if(handles->hkey != NULL)
		SKF_CloseHandle(handles->hkey);
	if(handles->hcont != NULL)
		SKF_CloseContainer(handles->hcont);
	if(handles->happ != NULL)
		SKF_CloseApplication(handles->happ);
	if(handles->hdev != NULL)
		SKF_DisConnectDev(handles->hdev);
	return 1;	
}

ULONG SM2_SV_setup(GM3000_SM2SV_Handles *handles){
	ULONG ulRslt = SAR_OK;
	
	char	szDevName[256] = {0};
	char	szAppName[256] = {0};
	char	szContName[256] = {0};
	char	*pUserPin = "123456";
	ULONG	ulDevNameLen = 256;
	ULONG	ulAppNameLen = 256;
	ULONG	ulContNameLen = 256;
	ULONG	ulRetryCount = 0;

	BLOCKCIPHERPARAM bp = {0};
	char *pappname = szAppName;
	char *pdevname = szDevName;
	char *pcontname = szContName;
	
	int i=0;
	ulRslt = SKF_EnumDev(TRUE, szDevName, &ulDevNameLen);
	if(ulRslt != SAR_OK){
		printf("%d\n",i);
		SM2_SV_destroy(handles);
	}
	
	i++; //1
	ulRslt = SKF_ConnectDev(pdevname, &handles->hdev);
	if(ulRslt != SAR_OK){
		printf("%d\n",i);
		SM2_SV_destroy(handles);
	}
	
	i++; //2
	ulRslt = SKF_EnumApplication(handles->hdev, szAppName, &ulAppNameLen);
	if(ulRslt != SAR_OK){
		printf("%d\n",i);
		SM2_SV_destroy(handles);
	}

	i++; //3 
	ulRslt = SKF_OpenApplication(handles->hdev, pappname, &handles->happ);
	//printf("app : %s\n",pappname);
	if(ulRslt != SAR_OK){
		printf("%d\n",i);
		SM2_SV_destroy(handles);
	}
	
	i++; //4
	ulRslt = SKF_VerifyPIN(handles->happ, USER_TYPE, pUserPin, &ulRetryCount);
	if(ulRslt != SAR_OK){
		printf("%d\n",i);
		SM2_SV_destroy(handles);
	}
	
	i++;  //5
	ulRslt = SKF_EnumContainer(handles->happ, szContName, &ulContNameLen);
	if(ulRslt != SAR_OK){
		printf("%d\n",i);
		SM2_SV_destroy(handles);
	}
	
	i++;  //6
	ulRslt = SKF_OpenContainer(handles->happ, pcontname, &handles->hcont);
	printf("container : %s\n",pcontname);
	if(ulRslt != SAR_OK){
		printf("%d\n",i);
		SM2_SV_destroy(handles);
	}
	
	BYTE pbCertificate[4096];
	ULONG pbCertLen = 4096;
	ulRslt = SKF_ExportCertificate(handles->hcont, TRUE, pbCertificate, &pbCertLen);
	printf("cerlen = %lu\n",pbCertLen);
	for(int j=0;j<pbCertLen;j++){
		printf("%02x",pbCertificate[j]);
	}
	printf("\n");
	
 	//生成密钥对
 	i++;  //7
 	ulRslt = SKF_GenECCKeyPair(handles->hcont, SGD_SM2_1, &handles->ecc_pub);
	if(ulRslt != SAR_OK){
		printf("%d\n",i);
		SM2_SV_destroy(handles);
	}
//	
	//先初始化公钥的长度，如果长度是默认值0，自然导出公钥报错 
//	i++;  //8 
//	handles->ulEccpubLen = sizeof(handles->ecc_pub);
//	ulRslt = SKF_ExportPublicKey(handles->hcont, TRUE, (BYTE *)&handles->ecc_pub, &handles->ulEccpubLen);
//	if(ulRslt != SAR_OK){
//		printf("%d\n",i);
//		SM2_SV_destroy(handles);
//	}
	
//	i++;
//	ulRslt = SKF_ImportECCKeyPair (handles->hcont, handles->key_pair);
//		if(ulRslt != SAR_OK){
//		printf("%d\n",i);
//		SM2_SV_destroy(handles);
//	}
	
	fflush(stdout);
	return ulRslt;
}

ULONG SM2_SV_sign(GM3000_SM2SV_Handles *handles,const char *srcfilename,const char *sigfilename){
	ULONG ulRslt = SAR_OK;
	
	//先摘要 
	BYTE digest[32];
	SM3_digest(handles,srcfilename,digest);
	
	//后签名 
	ulRslt = SKF_ECCSignData(handles->hcont, digest, sizeof(digest), &handles->ecc_sign);
	if(ulRslt == SAR_OK){
		printf("签名成功！\n");
	}else{
		printf("签名失败！\n");
		return SAR_FAIL;
	}
	//把公钥和签名值写入输出文件 
	FILE *fp = fopen(sigfilename,"wb");
	//fwrite(&handles->ecc_pub,1,handles->ulEccpubLen,fp);
	fwrite(&handles->ecc_sign,1,sizeof(handles->ecc_sign),fp);
	fflush(fp);
	fclose(fp);
	
	return ulRslt;
}

ULONG SM2_SV_verify(GM3000_SM2SV_Handles *handles,const char *srcfilename,const char *sigfilename){
	ULONG ulRslt = SAR_OK;
	
	//先摘要 
	BYTE digest[32];
	SM3_digest(handles,srcfilename,digest);
	
	//打开输入文件得到签名值 
	FILE *fp = fopen(sigfilename,"rb");
	ECCSIGNATUREBLOB signature;
	int n = fread(&signature,1,sizeof(signature),fp);
	printf("size of signature = %lu\n",signature);
	printf("read file bytes = %d\n",n);
	fclose(fp);
	
	//最后验签 
	ulRslt = SKF_ECCVerify(handles->hdev, &handles->key_pair->PubKey, digest, sizeof(digest), &signature);
	if(ulRslt == SAR_OK){
		printf("验签成功！\n");
	}else{
		printf("验签失败！\n");
		printf("error code: %02x\n",ulRslt);
	}
	return ulRslt;
}
int main(int argc,const char *argv[])
{
//	BYTE data[1024];
//	memset(data,0xE3,1024);

	GM3000_SM2SV_Handles handles;
	
	SM2_SV_setup(&handles);
	
	SM2_SV_sign(&handles,argv[2],argv[3]);
	
	//SM2_SV_verify(&handles,argv[2],argv[3]);
	
	SM2_SV_destroy(&handles);

	return 1;
}
