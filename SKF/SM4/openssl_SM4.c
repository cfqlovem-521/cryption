#include "openssl_SM4.h"
void hex_str_to_byte(const char *in, int len, unsigned char *out)
{
	char *str = (char *)malloc(len);
    memset(str, 0, len);
    memcpy(str, in, len);
    for (int i = 0; i < len; i+=2){
        //小写转大写
        if(str[i] >= 'a' && str[i] <= 'f'){
        	str[i] = str[i] - 0x20;
		}
        if(str[i+1] >= 'a' && str[i] <= 'f'){
        	str[i+1] = str[i+1] - 0x20;
		}
        //处理前4bit
        if(str[i] >= 'A' && str[i] <= 'F'){
        	out[i/2] = (str[i]-'A'+10)<<4;
		}else{
			out[i/2] = (str[i] & ~0x30)<<4;
		} 
        //处理后4bit, 并组合起来
        if(str[i+1] >= 'A' && str[i+1] <= 'F'){
        	out[i/2] |= (str[i+1]-'A'+10);
		}else{
			out[i/2] |= (str[i+1] & ~0x30);
		}
	}
	free(str);
}
//输入对称加密算法 和 加密模式
//输出密文到文件 
int SM4_encrypt(const EVP_CIPHER *type,const char *filename,const char *outfilename)
{
	FILE *fp1 = fopen(filename,"rb");
	FILE *fp2 = fopen(outfilename,"wb");
	
	//初始化 
	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
	EVP_CIPHER_CTX_init(ctx);
	EVP_EncryptInit_ex(ctx,type,NULL,NULL,NULL);
	
	//随机生成密钥
	EVP_CIPHER_CTX_rand_key(ctx,SM4Key);
	
	//判断模式
	unsigned char iv[IV_LEN];
	if(type != EVP_sm4_ecb()){
		//随机iv模式
		EVP_CIPHER_CTX_rand_key(ctx,iv);
		EVP_EncryptInit_ex(ctx,type,NULL,SM4Key,iv);
		//先在密文头部写入16字节的iv 
		fwrite(iv,1,IV_LEN,fp2);
#ifdef DEBUG
		printf("iv: ");
		for(int i=0;i<IV_LEN;i++){
			printf("%02x",iv[i]);
		}
		printf("\n");
#endif
	}else{
		EVP_EncryptInit_ex(ctx,type,NULL,SM4Key,NULL);
	}
	
	//读取明文，写入密文 
	unsigned char inbuf[1024] = {0};
	unsigned char outbuf[4096] = {0};
	int outlen;
	int inlen = 0;
	while((inlen = fread(inbuf,1,sizeof(inbuf),fp1))){
		EVP_EncryptUpdate(ctx,outbuf,&outlen,inbuf,inlen);
		fwrite(outbuf,1,outlen,fp2);
		fflush(fp2);
	}
	int finlen;
	EVP_EncryptFinal_ex(ctx,outbuf,&finlen);
	fwrite(outbuf,1,finlen,fp2);
	fflush(fp2);
	
	fclose(fp1);
	fclose(fp2);
	EVP_CIPHER_CTX_cleanup(ctx);
	return 1;
} 


int SM4_decrypt(const EVP_CIPHER *type,const char *filename,const char *outfilename)
{
	FILE *fp1 = fopen(filename,"rb");
	FILE *fp2 = fopen(outfilename,"wb");
	
	//上下文初始化 
	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
	EVP_CIPHER_CTX_init(ctx);
	
	//读出iv，解密初始化 
	unsigned char iv[IV_LEN];
	if(type != EVP_sm4_ecb()){
		fread(iv,1,IV_LEN,fp1);
		EVP_DecryptInit_ex(ctx,type,NULL,SM4Key,iv);
	}else{
		EVP_DecryptInit_ex(ctx,type,NULL,SM4Key,NULL);
	}
	
	//读写输入文件,输出文件 
	unsigned char inbuf[1024] = {0};
	unsigned char outbuf[4096] = {0};
	int outlen;
	int inlen = 0;
	while((inlen = fread(inbuf,1,sizeof(inbuf),fp1))){
	//	printf("inlen = %d\n",inlen);
		EVP_DecryptUpdate(ctx,outbuf,&outlen,inbuf,inlen);
		fwrite(outbuf,1,outlen,fp2);
		fflush(fp2);
	}
	int finlen;
	EVP_DecryptFinal_ex(ctx,outbuf,&finlen);
	fwrite(outbuf,1,finlen,fp2);
	fflush(fp2);
	
	fclose(fp1);
	fclose(fp2);
	EVP_CIPHER_CTX_cleanup(ctx);
	return 1;
}

int main(int argc,const char* argv[])
{
	if(strcmp(argv[2],"-d")==0){
		//解密 ./a.out cbc -d -K aaaaaaaaaaaaaa 1.enc 1.jpg
		hex_str_to_byte(argv[4],KEY_LEN*2,SM4Key);
		if(strcmp(argv[1],"cbc")==0){
			SM4_decrypt(EVP_sm4_cbc(),argv[5],argv[6]);
		}else if(strcmp(argv[1],"ecb")==0){
			SM4_decrypt(EVP_sm4_ecb(),argv[5],argv[6]);
		}else if(strcmp(argv[1],"cfb")==0){
			SM4_decrypt(EVP_sm4_cfb(),argv[5],argv[6]);
		}else if(strcmp(argv[1],"ofb")==0){
			SM4_decrypt(EVP_sm4_ofb(),argv[5],argv[6]);
		}else if(strcmp(argv[1],"ctr")==0){
			SM4_decrypt(EVP_sm4_ctr(),argv[5],argv[6]);
		}else{
			printf("usage:  ./a.out cbc -d -K aaaaaaaaaaaaaa infile outfile");
			printf("没有%s解密模式\n",argv[1]);
			return 1; 
		}
	}else{
		//加密 ./a.out cbc 1.jpg 2.jpg
		if(strcmp(argv[1],"cbc")==0){
			SM4_encrypt(EVP_sm4_cbc(),argv[2],argv[3]);
		}else if(strcmp(argv[1],"ecb")==0){
			SM4_encrypt(EVP_sm4_ecb(),argv[2],argv[3]);
		}else if(strcmp(argv[1],"cfb")==0){
			SM4_encrypt(EVP_sm4_cfb(),argv[2],argv[3]);
		}else if(strcmp(argv[1],"ofb")==0){
			SM4_encrypt(EVP_sm4_ofb(),argv[2],argv[3]);
		}else if(strcmp(argv[1],"ctr")==0){
			SM4_encrypt(EVP_sm4_ctr(),argv[2],argv[3]);
		}else{
			printf("usage:  ./a.out cbc infile outfile");
			printf("没有%s加密模式\n",argv[1]);
			return 2;
		}
	}
#ifdef DEBUG 
	printf("key: ");
		for(int i=0;i<KEY_LEN;i++){
			printf("%02x",SM4Key[i]);
		}
		printf("\n");
#endif 
	return 0; 
}
