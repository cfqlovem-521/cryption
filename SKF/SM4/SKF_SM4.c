#include "SKF_SM4.h"

ULONG SM_CIPHER_destroy(GM3000_Handles *handle){
    if(handle->hkey)
        SKF_CloseHandle(handle->hkey);
    if(handle->hcont)
        SKF_CloseContainer(handle->hcont);
    if(handle->happ)
        SKF_CloseApplication(handle->happ);
    if(handle->hdev)
        SKF_DisConnectDev(handle->hdev);
    return 1;
}

ULONG SM_CIPHER_setup(GM3000_Handles *handle,UINT cipher_mode,BYTE Random[KEY_LEN]){

    ULONG ulRslt = SAR_OK;

    char   szDevName[256] = {0};
    ULONG	ulDevNameLen = 256;
    char	szAppName[256] = {0};
    ULONG	ulAppNameLen = 256;
    char	szContName[256] = {0};
    ULONG	ulContName = 256;
    char	*pUserPin = "123456";
    ULONG	ulRetryCount = 0;
    BYTE    pbRandom[KEY_LEN] = {0};

    char *pContName = szContName;
    char *pdevname = szDevName;
    char *pappname = szAppName;

    ulRslt = SKF_EnumDev(TRUE, szDevName, &ulDevNameLen);
    if(ulRslt != SAR_OK) SM_CIPHER_destroy(handle);


    ulRslt = SKF_ConnectDev(pdevname, &handle->hdev);
    if(ulRslt != SAR_OK) SM_CIPHER_destroy(handle);

    ulRslt = SKF_EnumApplication(handle->hdev, szAppName, &ulAppNameLen);
    if(ulRslt != SAR_OK) SM_CIPHER_destroy(handle);


    ulRslt = SKF_OpenApplication(handle->hdev, pappname, &handle->happ);
    if(ulRslt != SAR_OK) SM_CIPHER_destroy(handle);

    ulRslt = SKF_VerifyPIN(handle->happ, USER_TYPE, pUserPin, &ulRetryCount);
    if(ulRslt != SAR_OK) SM_CIPHER_destroy(handle);

    ulRslt = SKF_EnumContainer(handle->happ, szContName, &ulContName);
    if(ulRslt != SAR_OK) SM_CIPHER_destroy(handle);


    ulRslt = SKF_OpenContainer(handle->happ, pContName, &handle->hcont);
    if(ulRslt != SAR_OK) SM_CIPHER_destroy(handle);

    if(Random == NULL){
        ulRslt = SKF_GenRandom(handle->hdev, pbRandom, KEY_LEN);
        if(ulRslt != SAR_OK) SM_CIPHER_destroy(handle);
        for(int i=0;i<KEY_LEN;i++){
            handle->key_Random[i] = pbRandom[i];
        }
        ulRslt = SKF_SetSymmKey(handle->hdev, pbRandom, cipher_mode, &handle->hkey);
        if(ulRslt != SAR_OK) SM_CIPHER_destroy(handle);
    }else{
        ulRslt = SKF_SetSymmKey(handle->hdev, Random, cipher_mode, &handle->hkey);
        if(ulRslt != SAR_OK) SM_CIPHER_destroy(handle);
    }

    return 1;
}

ULONG SM_CIPHER_encrypt(GM3000_Handles *handle,const char *infilename,const char *outfilename,BOOL iv){
    printf("encrypt function:\n");
    ULONG ulRslt = SAR_OK;
    
    //打开文件
    FILE *fp1 = fopen(infilename,"rb");
    FILE *fp2 = fopen(outfilename,"wb");
	
	//初始化，填充，设置iv 
	BLOCKCIPHERPARAM bp = {0}; 
    bp.PaddingType = 1;
    if(iv==TRUE){
    	bp.IVLen = IV_LEN;
   		ulRslt = SKF_GenRandom(handle->hdev, bp.IV, IV_LEN);
   		//把 iv 写到密文头部
   		fwrite(&bp.IV,1,IV_LEN,fp2);
	}
	
    ulRslt = SKF_EncryptInit(handle->hkey, bp);
	
	//读文件，循环update加密
    int inlen = 0;
    BYTE inbuf[BLK_SIZE];
    while((inlen = fread(inbuf,1,sizeof(inbuf),fp1))>0){
        ULONG outlen;
        ulRslt = SKF_EncryptUpdate(handle->hkey, inbuf, inlen, NULL, &outlen);
        BYTE *outbuf = (BYTE*)malloc(outlen);
        ulRslt = SKF_EncryptUpdate(handle->hkey, inbuf, inlen, outbuf, &outlen);
        fwrite(outbuf,1,outlen,fp2);
        fflush(fp2);
        free(outbuf);
        //printf("outlen = %d\n",outlen);
    }

	//final加密
    ULONG finlen;
    ulRslt = SKF_EncryptFinal(handle->hkey, NULL, &finlen);
    BYTE *finbuf = (BYTE*)malloc(finlen);
    ulRslt = SKF_EncryptFinal(handle->hkey, finbuf, &finlen);

    fwrite(finbuf,1,finlen,fp2);
    fflush(fp2);
    printf("finlen = %d\n",finlen);
    fflush(stdout);
	
	//释放资源
    fclose(fp1);
    fclose(fp2);

    free(finbuf);
    return ulRslt;
}

ULONG SM_CIPHER_decrypt(GM3000_Handles *handle,const char *infilename,const char *outfilename,BOOL iv){
    printf("decrypt function:\n");
    ULONG ulRslt = SAR_OK;
	
	//打开文件
    FILE *fp1 = fopen(infilename,"rb");
    FILE *fp2 = fopen(outfilename,"wb");
    
    //初始化，填充，设置iv 
	BLOCKCIPHERPARAM bp = {0}; 
    bp.PaddingType = 1;
    if(iv==TRUE){
    	bp.IVLen = IV_LEN;
   		fread(&bp.IV,1,IV_LEN,fp1);
	}
    ulRslt = SKF_DecryptInit(handle->hkey, bp);
	
	printf("file head iv : \t");
	for (int i=0;i<IV_LEN;i++){
		printf("%02x",bp.IV[i]);
	}
	printf("\n");
	
	//读取文件内容，循环update解密
    int inlen = 0;
    BYTE inbuf[BLK_SIZE];
    while((inlen = fread(inbuf,1,sizeof(inbuf),fp1))>0){
        ULONG outlen;
        ulRslt = SKF_DecryptUpdate(handle->hkey, inbuf, inlen, NULL, &outlen);
        BYTE *outbuf = (BYTE*)malloc(outlen);
        ulRslt = SKF_DecryptUpdate(handle->hkey, inbuf, inlen, outbuf, &outlen);
        fwrite(outbuf,1,outlen,fp2);
        fflush(fp2);
        free(outbuf);
        //printf("outlen = %d\n",outlen);
    }

	//final解密
    ULONG finlen;
    ulRslt = SKF_DecryptFinal(handle->hkey, NULL, &finlen);
    BYTE *finbuf = (BYTE*)malloc(finlen);
    ulRslt = SKF_DecryptFinal(handle->hkey, finbuf, &finlen);
    fwrite(finbuf,1,finlen,fp2);
    fflush(fp2);
    fflush(stdout);
	//释放资源
    fclose(fp1);
    fclose(fp2);
    free(finbuf);
    return ulRslt;
}

int main(int argc,const char *argv[]){
	
	ULONG ulRslt = SAR_OK;
    GM3000_Handles handle;
    BYTE key[KEY_LEN];
    BYTE iv[IV_LEN];
    
    printf("usage: ./a.out ecb -e(encrypt) infile outfile (-k key)\n");
    printf("usage: ./a.out cbc -d(decrypt) infile outfile (-k key)\n");

	if(strcmp(argv[1],"ecb")==0){
		//setup
		if(argv[6]==NULL){
			SM_CIPHER_setup(&handle,SGD_SM4_ECB,NULL);
		}else{
			HexString2Byte(argv[6], key);
			SM_CIPHER_setup(&handle,SGD_SM4_ECB,key);
		}
		//encrypt or decrypt
		if(strcmp(argv[2],"-e")==0){
    		ulRslt = SM_CIPHER_encrypt(&handle,argv[3],argv[4],FALSE);
		}else if(strcmp(argv[2],"-d")==0){
    		ulRslt = SM_CIPHER_decrypt(&handle,argv[3],argv[4],FALSE);
    	}
	}else if(strcmp(argv[1],"cbc")==0){
		//setup
		if(argv[6]==NULL){
			SM_CIPHER_setup(&handle,SGD_SM4_CBC,NULL);
		}else{
			HexString2Byte(argv[6], key);
			SM_CIPHER_setup(&handle,SGD_SM4_CBC,key);
		}
		//encrypt or decrypt
		if(strcmp(argv[2],"-e")==0){
    		ulRslt = SM_CIPHER_encrypt(&handle,argv[3],argv[4],TRUE);
		}else if(strcmp(argv[2],"-d")==0){
    		ulRslt = SM_CIPHER_decrypt(&handle,argv[3],argv[4],TRUE);
    	}
	}
	
	//Debug
	printf("input key:\t");
	for(int i=0;i<KEY_LEN;i++){
		printf("%02x",key[i]);
	}
	printf("\n");
		   
    SM_CIPHER_destroy(&handle);
	
	return 0;
}

