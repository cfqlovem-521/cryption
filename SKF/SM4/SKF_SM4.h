#ifndef SKF_SM4_H
#define SKF_SM4_H

#include "../skfapi.h" 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BLK_SIZE 4096
#define	TRUE	1
#define FALSE	0
#define KEY_LEN 16 	
#define IV_LEN 16

typedef unsigned char BYTE;

struct handles{
    HANDLE hdev;
    HANDLE happ;
    HANDLE hkey;
    HANDLE hcont;
    BYTE key_Random[KEY_LEN];
};

typedef struct handles GM3000_Handles;

ULONG SM_CIPHER_destroy(GM3000_Handles *handle);
ULONG SM_CIPHER_setup(GM3000_Handles *handle,UINT cipher_mode,BYTE Random[32]);
ULONG SM_CIPHER_encrypt(GM3000_Handles *handle,const char *infilename,const char *outfilename,BOOL iv);
ULONG SM_CIPHER_decrypt(GM3000_Handles *handle,const char *infilename,const char *outfilename,BOOL iv);

#endif // SKF_SM4_H
